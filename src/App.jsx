import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import TodoProvider from './components/todoProvider';
import TodoList from './components/TodoList';
import ErrorPage from './components/errorPage';

class App extends Component {
  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Route path="/error" exact component={ErrorPage} />
          <TodoProvider>
            <TodoList />
          </TodoProvider>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
