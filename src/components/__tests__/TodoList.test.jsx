/* eslint-disable no-undef */
import React from 'react';
import { mount } from 'enzyme';
import Todo from '../Todo';
import TodoList from '../TodoList';

let wrapper;
beforeEach(() => {
  wrapper = mount(
    <TodoList
      context={{
        todolist: [
          { task: 'test1' },
          { task: 'test2' },
        ],
      }}
    />,
  );
});

afterEach(() => {
  wrapper.unmount();
});

it('shows todo for each item in todolist', () => {
  expect(wrapper.find(Todo).length).toEqual(2);
});


// let wrapper;
// beforeEach(() => {

// });

// afterEach(() => {

// });

// it('shows Please add a task', () => {
//   let wrapper = mount(
//     <TodoList />,
//   );
//   expect(wrapper.render().text()).toContain('Please add a task');
// });
