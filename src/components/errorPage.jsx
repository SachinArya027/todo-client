import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div>
    <h3>Something went wrong</h3>
    <p>Please try again after sometime.</p>
    <div>
      <Link to="/">Go Home</Link>
    </div>
  </div>
);
