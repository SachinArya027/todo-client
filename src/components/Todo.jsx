import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withTodoContext from './HOC/withTodoContext';

class Todo extends Component {
  constructor(props) {
    super(props);

    const { todo: { completed, task } } = this.props;
    this.state = {
      editMode: false,
      completed: completed || false,
      task: task || '',
    };

    this.inputRef = React.createRef();
    this.updateTask = this.updateTask.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.completeTask = this.completeTask.bind(this);
  }

  componentDidUpdate() {
    const { editMode } = this.state;
    if (editMode) {
      this.inputRef.current.focus();
    }
  }

  updateTask() {
    const { context: { updateTodo }, todo: { id } } = this.props;
    const { task, completed } = this.state;
    updateTodo(id, {
      task,
      completed,
    });
  }

  completeTask() {
    this.setState(prevState => ({ completed: !prevState.completed }));
    this.updateTask();
  }

  deleteTask() {
    const { context: { deleteTodo }, todo: { id } } = this.props;
    deleteTodo(id);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  toggleEditMode() {
    const { editMode } = this.state;
    if (editMode) {
      const { todo: { task } } = this.props;
      this.setState({ editMode: false, task });
      return;
    }
    this.setState({ editMode: true });
  }

  renderTodo() {
    const { editMode, task, completed } = this.state;
    const textDecoration = completed ? { textDecoration: 'line-through' } : { textDecoration: 'none' };

    if (!editMode) {
      return (
        <div
          style={textDecoration}
          onClick={this.toggleEditMode}
          onKeyDown={this.toggleEditMode}
          role="button"
          tabIndex="0"
        >
          {task}
        </div>
      );
    }
    return (
      <div>
        <input
          ref={this.inputRef}
          type="text"
          name="task"
          onChange={this.handleChange}
          onBlur={this.toggleEditMode}
          value={task}
        />
        <button type="button">Update</button>
      </div>
    );
  }

  render() {
    const { completed } = this.state;
    return (
      <div className="todo" style={{ display: 'flex', alignItems: 'center' }}>
        <input
          ref={this.inputRef}
          type="checkbox"
          name="completed"
          defaultChecked={completed}
          onChange={this.completeTask}
        />
        {this.renderTodo()}
        <button type="button" onClick={this.deleteTask}>Delete</button>
      </div>
    );
  }
}

Todo.propTypes = {
  todo: PropTypes.shape({
    task: PropTypes.string.isRequired,
    updateTodo: PropTypes.func,
  }),
  context: PropTypes.shape({
    updateTodo: PropTypes.func,
  }),
};

Todo.defaultProps = {
  todo: PropTypes.object,
  context: PropTypes.any,
};

export default withTodoContext(Todo);
