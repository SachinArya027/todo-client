import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  Button,
  Header,
  Icon,
} from 'semantic-ui-react';
import withTodoContext from './HOC/withTodoContext';
import Todo from './Todo';
import AddTodo from './AddTodo';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = { showAddTaskModal: false };
    this.renderTodos = this.renderTodos.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal() {
    this.setState(prevState => ({ showAddTaskModal: !prevState.showAddTaskModal }));
  }

  renderTodos() {
    const { context: { todolist } } = this.props;
    if (Array.isArray(todolist) && todolist.length) {
      return todolist.map(todo => <Todo key={todo.id} todo={todo} />);
    }
    return 'Please add a task';
  }

  render() {
    const { showAddTaskModal } = this.state;
    return (
      <div>
        {this.renderTodos()}
        <hr />
        <Modal
          trigger={<Button onClick={this.toggleModal}>Add Task</Button>}
          open={showAddTaskModal}
          onClose={this.toggleModal}
          size="small"
        >
          <Header icon="browser" content="Add Task" />
          <Modal.Content>
            <p>Add new task here</p>
            <AddTodo />
          </Modal.Content>
          <Modal.Actions>
            <Button color="green" onClick={this.toggleModal} inverted>
              <Icon name="checkmark" />
              <span>Got it</span>
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

TodoList.propTypes = {
  context: PropTypes.shape({
    todolist: PropTypes.array.isRequired,
  }).isRequired,
};

export default withTodoContext(TodoList);
