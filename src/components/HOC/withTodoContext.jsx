import React from 'react';
import { TodoContext } from '../todoProvider';

export default WrappedComponent => props => (
  <TodoContext.Consumer>
    {state => <WrappedComponent {...props} context={state} />}
  </TodoContext.Consumer>
);
