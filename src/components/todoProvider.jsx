/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-console */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

export const TodoContext = React.createContext('todo');

class TodoProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todolist: [],
    };
    this.addTodo = this.addTodo.bind(this);
    this.updateTodo = this.updateTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);
  }

  componentDidMount() {
    this._loadAsyncData();
  }

  async addTodo(obj) {
    try {
      await axios.post('http://localhost:3001/api/todo', obj);
      this._loadAsyncData();
    } catch (err) {
      console.log(err);
      this.props.history.push('/error');
    }
  }

  async updateTodo(id, obj) {
    try {
      await axios.put(`http://localhost:3001/api/todo/${id}`, obj);
      this._loadAsyncData();
    } catch (err) {
      console.log(err);
      this.props.history.push('/error');
    }
  }

  async deleteTodo(id) {
    try {
      await axios.delete(`http://localhost:3001/api/todo/${id}`);
      this._loadAsyncData();
    } catch (err) {
      console.log(err);
      this.props.history.push('/error');
    }
  }

  async _loadAsyncData() {
    try {
      const response = await axios.get('http://localhost:3001/api/todos');
      this.setState({
        todolist: response.data,
      });
    } catch (err) {
      console.log(err);
      this.props.history.push('/error');
    }
  }

  render() {
    const { children } = this.props;
    const { todolist } = this.state;
    return (
      <TodoContext.Provider
        value={{
          todolist,
          addTodo: this.addTodo,
          updateTodo: this.updateTodo,
          deleteTodo: this.deleteTodo,
        }}
      >
        {children}
      </TodoContext.Provider>
    );
  }
}

TodoProvider.propTypes = {
  children: PropTypes.element.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
};

TodoProvider.defaultProps = {
  history: PropTypes.any,
};

export default withRouter(TodoProvider);
