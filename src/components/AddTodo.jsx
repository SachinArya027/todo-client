import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withTodoContext from './HOC/withTodoContext';

class AddToDo extends Component {
  constructor(props) {
    super(props);
    this.state = { task: '' };
    this.addNewTask = this.addNewTask.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async addNewTask() {
    const { context: { addTodo } } = this.props;
    const { task } = this.state;
    await addTodo({ task });
    this.setState({ task: '' });
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  render() {
    const { task } = this.state;
    return (
      <div>
        <input type="text" name="task" onChange={this.handleChange} value={task} />
        <button type="button" onClick={this.addNewTask}>Add</button>
      </div>
    );
  }
}

AddToDo.propTypes = {
  context: PropTypes.shape({
    addTodo: PropTypes.func.isRequired,
  }).isRequired,
};

export default withTodoContext(AddToDo);
